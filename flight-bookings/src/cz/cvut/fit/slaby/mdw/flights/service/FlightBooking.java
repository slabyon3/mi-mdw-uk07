package cz.cvut.fit.slaby.mdw.flights.service;

import cz.cvut.fit.slaby.mdw.flights.Flight;
import cz.cvut.fit.slaby.mdw.flights.Booking;
import cz.cvut.fit.slaby.mdw.flights.FlightDB;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeParser;
import org.joda.time.format.ISODateTimeFormat;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@WebService
public class FlightBooking {
    @WebMethod
    public String addBooking(@WebParam(name = "name") String name,
                             @WebParam(name = "start") String fromAirport,
                             @WebParam(name = "destination") String toAirport,
                             @WebParam(name = "departureTime") String departure,
                             @WebParam(name = "arrivalTime") String arrival) {
        FlightDB db = FlightDB.getInstance();

        DateTimeFormatter fmt = ISODateTimeFormat.dateHourMinute();

        DateTime depTime = null;
        DateTime arrTime = null;

        try {
            depTime = fmt.parseDateTime(departure);
            arrTime = fmt.parseDateTime(arrival);
        } catch (IllegalArgumentException e) {
            return "Invalid time/date!";
        }

        Flight flight = db.getFlight(fromAirport, toAirport, depTime, arrTime);
        if (flight == null) {
            return "No such flight found!";
        } else {
            return addBooking(name, flight);
        }
    }

    /*  For use in the 8th homework
    @WebMethod
    public String addBooking(@WebParam(name = "name") String name,
                             @WebParam(name = "destination") String destination) {
        FlightDB db = FlightDB.getInstance();

        Flight flight = db.findFlightTo(destination);
        if (flight == null) {
            return "No such flight found!";
        } else {
            return addBooking(name, flight);
        }
    }
    */

    private String addBooking(String name, Flight flight) {
        FlightDB db = FlightDB.getInstance();
        Booking booking = new Booking(name, flight);
        db.addBooking(booking);
        return booking.toString();
    }

    @WebMethod
    public List<String> listBookings() {
        FlightDB db = FlightDB.getInstance();
        Collection<Booking> bookings = db.getAllBookings();

        List<String> result = new ArrayList<>();
        for (Booking booking : bookings) {
            result.add(booking.toString());
        }
        return result;
    }

    @WebMethod
    public String updateBooking(@WebParam(name = "bookingId") int bookingId,
                                @WebParam(name = "newName") String newName) {
        FlightDB db = FlightDB.getInstance();

        Booking booking = db.findBooking(bookingId);
        if (booking == null) {
            return "No such booking found!";
        } else {
            booking.setPassengerName(newName);
            return booking.toString();
        }
    }

    @WebMethod
    public String deleteBooking(@WebParam(name = "bookingId") int bookingId) {
        FlightDB db = FlightDB.getInstance();

        Booking booking = db.findBooking(bookingId);
        if (booking == null) {
            return "No such booking found!";
        } else {
            db.removeBooking(booking);
            return booking.toString();
        }
    }
}
