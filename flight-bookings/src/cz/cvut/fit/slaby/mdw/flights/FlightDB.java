package cz.cvut.fit.slaby.mdw.flights;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.*;

public class FlightDB {
    private static FlightDB instance;

    private Map<Flight, List<Booking>> flights;
    private Map<Integer, Booking> bookings;

    public static FlightDB getInstance() {
        if (instance == null) instance = new FlightDB();
        return instance;
    }

    public FlightDB() {
        flights = new HashMap<>();
        bookings = new HashMap<>();

        DateTimeFormatter fmt = ISODateTimeFormat.dateHourMinute();
        flights.put(
                new Flight("Prague", "Budapest", fmt.parseDateTime("2017-12-20T12:37"), fmt.parseDateTime("2017-12-20T15:42")),
                new ArrayList<>()
        );
        flights.put(
                new Flight("Prague", "Paris", fmt.parseDateTime("2017-12-20T11:20"), fmt.parseDateTime("2017-12-20T13:40")),
                new ArrayList<>()
        );
    }

    public Flight getFlight(String fromAirport, String toAirport, DateTime departure, DateTime arrival) {
        Flight flight = new Flight(fromAirport, toAirport, departure, arrival);
        if (flights.containsKey(flight)) {
            return flight;
        } else {
            return null;
        }
    }

    public Flight findFlightTo(String destination) {
        for (Flight flight : flights.keySet()) {
            if (flight.getDestAirport().equals(destination)) {
                return flight;
            }
        }
        return null;
    }

    public void addBooking(Booking booking) {
        bookings.put(booking.getId(), booking);
        flights.get(booking.getFlight()).add(booking);
    }

    public Collection<Booking> getAllBookings() {
        return bookings.values();
    }

    public Booking findBooking(int bookingId) {
        return bookings.getOrDefault(bookingId, null);
    }

    public void removeBooking(Booking booking) {
        bookings.remove(booking.getId());
        flights.get(booking.getFlight()).remove(booking);
    }
}
