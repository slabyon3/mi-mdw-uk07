package cz.cvut.fit.slaby.mdw.flights;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;

public class Flight {
    private DateTime departure;
    private DateTime arrival;
    private String srcAirport;
    private String destAirport;

    public Flight(String srcAirport, String destAirport, DateTime departure, DateTime arrival) {
        this.departure = departure;
        this.arrival = arrival;
        this.srcAirport = srcAirport;
        this.destAirport = destAirport;
    }

    public DateTime getDeparture() {
        return departure;
    }

    public DateTime getArrival() {
        return arrival;
    }

    public String getSrcAirport() {
        return srcAirport;
    }

    public String getDestAirport() {
        return destAirport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (departure != null ? !departure.equals(flight.departure) : flight.departure != null) return false;
        if (arrival != null ? !arrival.equals(flight.arrival) : flight.arrival != null) return false;
        if (srcAirport != null ? !srcAirport.equals(flight.srcAirport) : flight.srcAirport != null) return false;
        return destAirport != null ? destAirport.equals(flight.destAirport) : flight.destAirport == null;
    }

    @Override
    public int hashCode() {
        int result = departure != null ? departure.hashCode() : 0;
        result = 31 * result + (arrival != null ? arrival.hashCode() : 0);
        result = 31 * result + (srcAirport != null ? srcAirport.hashCode() : 0);
        result = 31 * result + (destAirport != null ? destAirport.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "flight from " +
                srcAirport.toString() +
                " (departure: " +
                departure.toString(ISODateTimeFormat.dateHourMinute()) +
                ") to " +
                destAirport.toString() +
                " (arrival: " +
                arrival.toString(ISODateTimeFormat.dateHourMinute()) +
                ")";
    }
}
