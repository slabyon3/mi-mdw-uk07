package cz.cvut.fit.slaby.mdw.flights;

public class Booking {
    private static int nextId = 0;
    private int id;
    private String passengerName;
    private Flight flight;

    public Booking(String passengerName, Flight flight) {
        this.passengerName = passengerName;
        this.flight = flight;
        this.id = nextId++;
    }

    public int getId() {
        return id;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public Flight getFlight() {
        return flight;
    }

    @Override
    public String toString() {
        return "booking id: " +
                String.valueOf(id) +
                " for " +
                flight.toString() +
                " for name: " +
                passengerName.toString();
    }
}
